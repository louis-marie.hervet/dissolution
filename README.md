## Description

On essaie de prédire la composition de l'Assemblée Nationale 2024 après la dissolution

## Equipe

- Sacha KRETZ
- Louis-Marie HERVET

## Idées des étapes de résolution
- Récupérer les résultats par circonscription : Candidat / Pourcentage des suffrages
- Connaître les candidats sortants grâce aux résultats du 2e tour
- Prédire les candidats 2024 en prenant en compte les alliances annoncées dans la presse
- Prédire les résultats 1er puis 2e tour 2024 à partir de la liste de candidats
- Comparer la composition 2022 et la composition 2024 de l'Assemblée Nationale 

